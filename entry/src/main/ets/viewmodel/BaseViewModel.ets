import { AxiosResponse } from '@ohos/axios'
import useRequestStatus from '../http/useRequestStatus'
import { OrderListItemData } from '../model/OrderModel'
import { WinRecordListItemData } from '../model/WinRecordModel'
export type StateCallback = (state: string) => void

export type resultType = OrderListItemData[] |WinRecordListItemData[]| undefined | AxiosResponse
export type ResultCallback = (result: resultType) => void

export class BaseViewModel {
  private stateCallback?: StateCallback

  observeState(stateCallback: StateCallback) {
    this.stateCallback = stateCallback
  }

  dispatchViewState(state: string) {
    this.stateCallback?.(state)
  }

  httpRequest(checkResult?: boolean) {
    const fn = (state:string): void => this.dispatchViewState(state)
    return useRequestStatus(fn, checkResult)
  }
}