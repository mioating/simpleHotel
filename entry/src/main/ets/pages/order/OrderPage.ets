/**
 * 订单功能
 */
import { OrderViewModel, TabItem } from '../../viewmodel/OrderViewModel'
import { CommonTopBar } from '../../views/CommonTopBar'
import { OrderList } from './OrderList'

@Entry
@Component
struct OrderPage {
  @Provide hotCurrentIndex: number = 0
  private controller: TabsController = new TabsController()
  private viewModel = new OrderViewModel()

  build() {
    Stack() {
      Column() {
        CommonTopBar({ title: "我的订单", backButton: true })
        Tabs({ barPosition: BarPosition.Start, controller: this.controller }) {
          ForEach(this.viewModel.tabList, (tabItem: TabItem, index) => {
            TabContent() {
              OrderList({ tabKey: tabItem.key })
            }.tabBar(this.TabBuilder(index, tabItem.title)).padding({ top: 0, bottom: 0 }).width("100%")
          })
        }
        .barHeight($r("app.float.size_50"))
        .barWidth("100%")
        .barMode(BarMode.Fixed)
        .margin({ bottom: 50 })
        .onChange((index) => {
          this.hotCurrentIndex = index
        })
      }.backgroundColor($r('app.color.color_bg'))
    }
  }
  @Builder TabBuilder(index: number, name: string) {
    Column() {
      Text(name)
        .fontColor(this.hotCurrentIndex === index ? $r("app.color.color_theme_main") : $r("app.color.color_999"))
        .fontSize($r('app.float.size_text_15'))
        .fontWeight(this.hotCurrentIndex === index ? 500 : 400)
        .textAlign(TextAlign.Center)
        .height($r("app.float.size_40"))
      Divider()
        .strokeWidth(2)
        .color($r("app.color.color_theme_main"))
        .opacity(this.hotCurrentIndex === index ? 1 : 0)
        .width($r("app.float.size_text_80"))
        .lineCap(LineCapStyle.Round)
    }
    .width('101%') //解决Tab连接处白色线条间隙
    .height($r("app.float.size_50"))
    .justifyContent(FlexAlign.End)
  }
}